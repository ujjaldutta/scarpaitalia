<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'scarpaItalia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'user123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9d8v>b!opVXD`{Aq2-|.IVHl#EBs<+|anC1Rd f?/%(Av>EabuebuvU+q<tNF/-r');
define('SECURE_AUTH_KEY',  'gj%ALOqOTO~+iMT]14bf;3T9w)JbH1qwyUOI7]depn`+bB9;OBVb<w[NrqEgGhBU');
define('LOGGED_IN_KEY',    '^>FxkDj+T^2&to>=I}n.60~X4{|n$@H:_S)FfDveZz)R-g1jZL&{RIVXFF>yUM}3');
define('NONCE_KEY',        'a;~t{vL|O?rN=Eq{j--4]-RZs*<lU#[za!R1+R/)FI>f&FmyrYOk>7$v!{?|~T#2');
define('AUTH_SALT',        '&$FazMmQxP|CQu9OK-MTlt2C{EDu7E[DN=fnmL+{okkY|cW);l8-<|:60|*iee6,');
define('SECURE_AUTH_SALT', '}.ar=I{+O7%#Cbv+{,KmNS;t_68F0+ICL!|Q?av[hh~S(02]Erctcc-2^h>IITvP');
define('LOGGED_IN_SALT',   'J}SmYaFhO)O/N+e4Wc!G]m/C}8 wA+i]*_A^Tdlo/W>@bl;j9q^uob5Um)wJi_WV');
define('NONCE_SALT',       'o0l!G(rOXrO$cUy!-sGwYGe:V8jABs}7SN~c-Iv.;H@YxJ*hjd6S=}LTh7[(wGNR');
define('FS_METHOD', 'direct');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
