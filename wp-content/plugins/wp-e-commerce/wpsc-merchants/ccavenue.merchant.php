<?php

/**
	* WP eCommerce Test Merchant Gateway
	* This is the file for the test merchant gateway
	*
	* @package wp-e-comemrce
	* @since 3.7.6
	* @subpackage wpsc-merchants
*/
$nzshpcrt_gateways[$num] = array(
	'name' => __( 'CCAvenue', 'wpsc' ),
	'api_version' => 2.0,
	'class_name' => 'wpsc_merchant_ccavenue',
	'image' => WPSC_URL . '/images/cc-avenue.png',
	'has_recurring_billing' => true,
	'display_name' => __( 'CCAvenue Payment', 'wpsc' ),
	'wp_admin_cannot_cancel' => false,
	'requirements' => array(
		 /// so that you can restrict merchant modules to PHP 5, if you use PHP 5 features
		///'php_version' => 5.0,
	),
	'function' => 'gateway_ccavenue',
	'form' => 'form_ccavenue',
	'internalname' => 'wpsc_merchant_ccavenue',
	'submit_function' => 'submit_ccavenue',
	'payment_type' => 'ccavenue',
	'supported_currencies' => array(
		'currency_list' =>  array('AUD', 'BRL', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MYR', 'MXN', 'NOK', 'NZD', 'PHP', 'PLN', 'GBP', 'RUB', 'SGD', 'SEK', 'CHF', 'TWD', 'THB', 'TRY', 'USD','INR'),
		'option_name' => 'ccavenue_curcode'
	)
);

$image = apply_filters( 'wpsc_merchant_image', '', $nzshpcrt_gateways[$num]['internalname'] );
if ( ! empty( $image ) ) {
	$nzshpcrt_gateways[$num]['image'] = $image;
}
class wpsc_merchant_ccavenue extends wpsc_merchant {

	var $name = '';

	function __construct( $purchase_id = null, $is_receiving = false ) {
		$this->name = __( 'CCAvenue Payment', 'wpsc' );
		parent::__construct( $purchase_id, $is_receiving );
	}

	function submit() {
		
		global $wpdb;
		/*$purchase_log_sql = $wpdb->prepare( "SELECT * FROM `".WPSC_TABLE_PURCHASE_LOGS."` WHERE `sessionid`= %s LIMIT 1", $sessionid );
		$purchase_log = $wpdb->get_results($purchase_log_sql,ARRAY_A) ;
	
		$cart_sql = "SELECT * FROM `".WPSC_TABLE_CART_CONTENTS."` WHERE `purchaseid`='".$purchase_log[0]['id']."'";
		$cart = $wpdb->get_results($cart_sql,ARRAY_A) ;*/
	
		// ChronoPay post variables
		$ccavenue_url = get_option('ccavenue_url');
	
		$data['Merchant_Id'] = get_option('merchant_id');
		$data['Redirect_Url'] = add_query_arg( 'ccavenue_callback', 'true', home_url( '/' ) );
		
		$data['Order_Id'] = $this->cart_data['session_id'];
		$data['Currency'] = $this->get_local_currency_code();
		$data['actionID'] = "TXN";
		$data['TxnType'] = "A";
	
		// User details
		if($_POST['collected_data'][get_option('ccavenue_form_first_name')] != '')
	    {
		$data['billing_cust_name'] = $_POST['collected_data'][get_option('ccavenue_form_first_name')];
	    }
		if($_POST['collected_data'][get_option('ccavenue_form_last_name')] != "")
	    {
		$data['billing_cust_name'] = $data['billing_cust_name']. " ".$_POST['collected_data'][get_option('ccavenue_form_last_name')];
	    }
		if($_POST['collected_data'][get_option('ccavenue_form_address')] != '')
	    {
		$data['billing_cust_address'] = str_replace("\n",', ', $_POST['collected_data'][get_option('ccavenue_form_address')]);
	    }
		if($_POST['collected_data'][get_option('ccavenue_form_city')] != '')
	    {
		$data['billing_cust_city'] = $_POST['collected_data'][get_option('ccavenue_form_city')];
	    }
		
		if($_POST['collected_data'][get_option('ccavenue_form_state')] != '')
	    {
		$data['billing_cust_state'] = $_POST['collected_data'][get_option('ccavenue_form_state')];
	    }
	    
		$data['billing_cust_country'] = (string) wpsc_get_customer_meta( 'billingcountry' );
		
		if($_POST['collected_data'][get_option('ccavenue_form_post_code')] != '')
	    {
		$data['billing_zip'] = $_POST['collected_data'][get_option('ccavenue_form_post_code')];
	    }
		
		if($_POST['collected_data'][get_option('ccavenue_form_phone')] != '')
	    {
		$data['billing_cust_tel'] = $_POST['collected_data'][get_option('ccavenue_form_phone')];
	    }
	    
	    
	    
		// Change suggested by waxfeet@gmail.com, if email to be sent is not there, dont send an email address
		$email_data = $wpdb->get_results("SELECT `id`,`type` FROM `".WPSC_TABLE_CHECKOUT_FORMS."` WHERE `type` IN ('email') AND `active` = '1'",ARRAY_A);
		foreach((array)$email_data as $email)
	    {
		$data['billing_cust_email'] = $_POST['collected_data'][$email['id']];
	    }
		if(($_POST['collected_data'][get_option('email_form_field')] != null) && ($data['email'] == null))
	    {
		$data['billing_cust_email'] = $_POST['collected_data'][get_option('email_form_field')];
	    }
	    
	    //shipping detail will go here
	    
	    
	    if($_POST['collected_data'][get_option('ccavenue_form_shipping_first_name')] != '')
	    {
		$data['delivery_cust_name'] = $_POST['collected_data'][get_option('ccavenue_form_shipping_first_name')];
	    }
		if($_POST['collected_data'][get_option('ccavenue_form_shipping_last_name')] != "")
	    {
		$data['delivery_cust_name'] = $data['delivery_cust_name']. " ".$_POST['collected_data'][get_option('ccavenue_form_shipping_last_name')];
	    }
		if($_POST['collected_data'][get_option('ccavenue_form_shipping_address')] != '')
	    {
		$data['delivery_cust_address'] = str_replace("\n",', ', $_POST['collected_data'][get_option('ccavenue_form_shipping_address')]);
	    }
		if($_POST['collected_data'][get_option('ccavenue_form_shipping_city')] != '')
	    {
		$data['delivery_cust_city'] = $_POST['collected_data'][get_option('ccavenue_form_shipping_city')];
	    }
		
		if($_POST['collected_data'][get_option('ccavenue_form_shipping_state')] != '')
	    {
		$data['delivery_cust_state'] = $_POST['collected_data'][get_option('ccavenue_form_shipping_state')];
	    }
	    
	    if($_POST['collected_data'][get_option('ccavenue_form_shipping_country')] != '')
	    {
		$data['delivery_cust_country'] = $_POST['collected_data'][get_option('ccavenue_form_shipping_country')];
	    }
	    
		
		
		if($_POST['collected_data'][get_option('ccavenue_form_shipping_post_code')] != '')
	    {
		$data['delivery_zip_code'] = $_POST['collected_data'][get_option('ccavenue_form_shipping_post_code')];
	    }
		
		if($_POST['collected_data'][get_option('ccavenue_form_shipping_phone')] != '')
	    {
		$data['delivery_cust_tel'] = $_POST['collected_data'][get_option('ccavenue_form_shipping_phone')];
	    }
	    
	    
	    
	    
	    
	
		// Get Currency details abd price
		$local_currency_code = $this->get_local_currency_code();
		//$local_currency_code = $currency_code[0]['code'];
		$ccavenue_currency_code = get_option('ccavenue_curcode');
	
		// ChronoPay only processes in the set currency.  This is USD or EUR dependent on what the Chornopay account is set up with.
		// This must match the ChronoPay settings set up in wordpress.  Convert to the chronopay currency and calculate total.
		$curr=new CURRENCYCONVERTER();
		$decimal_places = 2;
		$total_price = 0;

		$i = 1;
	
		// Cart Item Data
		$i = $item_total = 0;
		$tax_total = wpsc_tax_isincluded() ? 0 : $this->cart_data['cart_tax'];

		$shipping_total = $this->convert( $this->cart_data['base_shipping'] );

		foreach ( $this->cart_items as $cart_row ) {
			$data['L_NAME' . $i] = apply_filters( 'the_title', $cart_row['name'] );
			$data['L_AMT' . $i] = $this->convert( $cart_row['price'] );
			$data['L_NUMBER' . $i] = $i;
			$data['L_QTY' . $i] = $cart_row['quantity'];

			$shipping_total += $this->convert( $cart_row['shipping'] );
			$item_total += $this->convert( $cart_row['price'] ) * $cart_row['quantity'];

			$i++;
		}

		if ( $this->cart_data['has_discounts'] ) {
			$discount_value = $this->convert( $this->cart_data['cart_discount_value'] );

			$coupon = new wpsc_coupons( $this->cart_data['cart_discount_data'] );

			// free shipping
			if ( $coupon->is_percentage == 2 ) {
				$shipping_total = 0;
				$discount_value = 0;
			} elseif ( $discount_value >= $item_total ) {
				$discount_value = $item_total - 0.01;
				$shipping_total -= 0.01;
			}

			$data["L_NAME{$i}"] = _x( 'Coupon / Discount', 'PayPal Pro Item Name for Discounts', 'wpsc' );
			$data["L_AMT{$i}"] = - $discount_value;
			$data["L_NUMBER{$i}"] = $i;
			$data["L_QTY{$i}"] = 1;
			$item_total -= $discount_value;
		}

		// Cart totals
		$data['ITEMAMT'] = $this->format_price( $item_total );
		$data['SHIPPINGAMT'] = $this->format_price( $shipping_total );
		$data['Tax'] = $this->convert( $tax_total );
		$data['Amount'] = $data['ITEMAMT'] + $data['SHIPPINGAMT'] + $data['TAXAMT'];
		
		$data['Checksum'] = $this->getchecksum($data['Merchant_Id'],$data['Amount'] ,$data['Order_Id'],$data['Redirect_Url'],get_option('working_key'));
		
		
				
	
	
		// Create Form to post to ChronoPay
		$output = "
			<form id=\"ccavenue_form\" name=\"ccavenue_form\" method=\"post\" action=\"$ccavenue_url\">\n";
	
		foreach($data as $n=>$v) {
				$output .= "			<input type=\"hidden\" name=\"$n\" value=\"$v\" />\n";
		}
	
		$output .= "			<input type=\"submit\" value=\"Continue to CCAvenue\" />
			</form>
		";
	
		// echo form..
		if( get_option('ccavenue_debug') == 1)
		{
			echo ("DEBUG MODE ON!!<br/>");
			echo("The following form is created and would be posted to ChronoPay for processing.  Press submit to continue:<br/>");
			echo("<pre>".htmlspecialchars($output)."</pre>");
			
			echo("Form submit data:<br/>");
			echo("<pre>".print_r($data,true)."</pre>");
			echo("Cart  data:<br/>");
			exit("<pre>".print_r($this->cart_items,true)."</pre>");
		}
	
		echo($output);
	
		if(get_option('ccavenue_debug') == 0)
		{
			echo "<script language=\"javascript\" type=\"text/javascript\">document.getElementById('ccavenue_form').submit();</script>";
		}
	
		exit();

	}
	
	
	function get_local_currency_code() {
		if ( empty( $this->local_currency_code ) ) {
			$this->local_currency_code = WPSC_Countries::get_currency_code( get_option( 'currency_type' ) );
		}

		return $this->local_currency_code;
	}
	
	
	
	
	function convert( $amt ) {
		if ( empty( $this->rate ) ) {
			$this->rate = 1;
			$cc_currency_code = get_option( 'ccavenue_curcode' );
			$local_currency_code = $this->get_local_currency_code();
			if ( $local_currency_code != $cc_currency_code ) {
				$curr = new CURRENCYCONVERTER();
				$this->rate = $curr->convert( 1, $cc_currency_code, $local_currency_code );
			}
		}

		return $this->format_price( $amt * $this->rate );
	}
	
	function format_price( $price, $cc_currency_code = null ) {
		if ( ! isset( $cc_currency_code ) ) {
			$cc_currency_code = get_option( 'ccavenue_curcode' );
		}
		switch( $cc_currency_code ) {
			case "JPY":
				$decimal_places = 0;
				break;

			case "HUF":
				$decimal_places = 0;

			default:
				$decimal_places = 2;
				break;
		}
		$price = number_format( sprintf( "%01.2f", $price ), $decimal_places, '.', '' );
		return $price;
	}
		
	/*
	ccavenue functions
	 */
	function getchecksum($MerchantId,$Amount,$OrderId ,$URL,$WorkingKey)
	{
		    $str ="$MerchantId|$OrderId|$Amount|$URL|$WorkingKey";
		    $adler = 1;
		    $adler = $this->adler32($adler,$str);
		    return $adler;
	}
	function verifychecksum($MerchantId,$OrderId,$Amount,$AuthDesc,$CheckSum,$WorkingKey)
	{
		    $str = "$MerchantId|$OrderId|$Amount|$AuthDesc|$WorkingKey";
		    $adler = 1;
		    $adler = $this->adler32($adler,$str);
		    if($adler == $CheckSum)
				return "true" ;
		    else
				return "false" ;
	}
	function adler32($adler , $str)
	{
		    $BASE =  65521 ;
		    $s1 = $adler & 0xffff ;
		    $s2 = ($adler >> 16) & 0xffff;
		    for($i = 0 ; $i < strlen($str) ; $i++)
		    {
				$s1 = ($s1 + Ord($str[$i])) % $BASE ;
				$s2 = ($s2 + $s1) % $BASE ;
					    
		    }
		    return $this->leftshift($s2 , 16) + $s1;
	}
	 //leftshift function
	function leftshift($str , $num)
	{
		$str = DecBin($str);
		for( $i = 0 ; $i < (64 - strlen($str)) ; $i++)
		$str = "0".$str ;
		for($i = 0 ; $i < $num ; $i++)
		{
		$str = $str."0";
		$str = substr($str , 1 ) ;
		}
		return $this->cdec($str) ;
	}
	 //cdec function
	function cdec($num)
	{
		for ($n = 0 ; $n < strlen($num) ; $n++)
		{
		$temp = $num[$n] ;
		$dec = $dec + $temp*pow(2 , strlen($num) - $n - 1);
		}
		return $dec;
	}
}


function submit_ccavenue(){
	
	if(isset($_POST['ccavenue_url']))
		{
		    update_option('ccavenue_url', $_POST['ccavenue_url']);
		}
	
	if(isset($_POST['ccavenue_payinstructions']))
		{
		    update_option('ccavenue_payinstructions', $_POST['ccavenue_payinstructions']);
		}
		
	if(isset($_POST['merchant_id']))
		{
		    update_option('merchant_id', $_POST['merchant_id']);
		}
	
	if(isset($_POST['working_key']))
		{
		    update_option('working_key', $_POST['working_key']);
		}
	
	if(isset($_POST['access_code']))
		{
		    update_option('access_code', $_POST['access_code']);
		}
		
	if(isset($_POST['ccavenue_debug']))
		{
		    update_option('ccavenue_debug', $_POST['ccavenue_debug']);
		}
		
	if (!isset($_POST['ccavenue_form'])) $_POST['ccavenue_form'] = array();
		foreach((array)$_POST['ccavenue_form'] as $form => $value)
	    {
		update_option(('ccavenue_form_'.$form), $value);
	    }
	    
	    	if(isset($_POST['ccavenue_curcode']))
		{
		    update_option('ccavenue_curcode', $_POST['ccavenue_curcode']);
		}
	
}
function form_ccavenue() {
	$select_currency[get_option('chronopay_curcode')] = "selected='selected'";	
	$ccavenue_debug = get_option('ccavenue_debug');
	$ccavenue_debug1 = "";
	$ccavenue_debug2 = "";
	switch($ccavenue_debug)
	{
		case 0:
			$ccavenue_debug2 = "checked ='checked'";
			break;
		case 1:
			$ccavenue_debug1 = "checked ='checked'";
			break;
	}
	
	
	
	if (!isset($select_currency['USD'])) $select_currency['USD'] = '';
	if (!isset($select_currency['EUR'])) $select_currency['EUR'] = '';
	if (!isset($select_language['INR'])) $select_language['INR'] = '';
	
	
	$output ='<tr><td>
				 '. __( 'CCAvenue Url :', 'wpsc' ) . '
			</td><td>';
	$output.=' <input type="textbox" value="'.get_option( 'ccavenue_url' ).'" name="wpsc_options[ccavenue_url]" size="70"/>';
	$output .='</td></tr>';
	
	$output .='<tr><td>
				 '. __( 'Payment Instructions :', 'wpsc' ) . '
			</td><td>';
	$output.=' <textarea cols="40" rows="9" name="wpsc_options[ccavenue_payinstructions]">' . esc_textarea( get_option( 'ccavenue_payinstructions' ) ) . '</textarea><br />';
	$output .='
			<p class="description">
					'.__('For example, this is where you the Shop Owner might enter your bank account details or address so that your customer can make their manual payment.', 'wpsc').'
				</p>
	</td></tr>';
	
	$output .='<tr><td>
				 '. __( 'Merchant ID :', 'wpsc' ) . '
			</td><td>';
	$output.=' <input type="textbox" value="'.get_option( 'merchant_id' ).'" name="wpsc_options[merchant_id]" size="30"/>';
	$output .='</td></tr>';
	
	$output .='<tr><td>
				 '. __( 'Working Key :', 'wpsc' ) . '
			</td><td>';
	$output.=' <input type="textbox" value="'.get_option( 'working_key' ).'" name="wpsc_options[working_key]" size="30"/>';
	$output .='</td></tr>';
	
	$output .='<tr><td>
				 '. __( 'Access Code :', 'wpsc' ) . '
			</td><td>';
	$output.=' <input type="textbox" value="'.get_option( 'access_code' ).'" name="wpsc_options[access_code]" size="30"/>';
	$output .='</td></tr>';
	$output .="<tr>
			<td>" . __( 'Debug Mode', 'wpsc' ) . "</td>
			<td>
				<input type='radio' value='1' name='ccavenue_debug' id='ccavenue_debug1' " . $ccavenue_debug1 . " /> <label for='ccavenue_debug1'>".__('Yes', 'wpsc')."</label> &nbsp;
				<input type='radio' value='0' name='ccavenue_debug' id='ccavenue_debug2' " . $ccavenue_debug2 . " /> <label for='ccavenue_debug2'>".__('No', 'wpsc')."</label>
				<p class='description'>
					" . __( 'Debug mode is used to write HTTP communications between the ccavenue server and your host to a log file.  This should only be activated for testing!', 'wpsc' ) . "
				</p>
		</tr>";
		
		
		
	$output .="<tr>
			<td>" . __( 'Accepted Currency', 'wpsc' ) . "</td>
			<td>
				<select name='ccavenue_curcode'>
					<option " . $select_currency['USD'] . " value='USD'>" . __( 'USD - U.S. Dollar', 'wpsc' ) . "</option>
					<option " . $select_currency['EUR'] . " value='EUR'>" . __( 'EUR - Euros', 'wpsc' ) . "</option>
					<option " . $select_currency['INR'] . " value='INR'>" . __( 'INR - Rupees', 'wpsc' ) . "</option>
				</select>
				<p class='description'>
					" . __( 'The currency code that ChronoPay will process the payment in. All products must be set up in this currency.', 'wpsc' ) . "
				</p>
		</tr>";	
		
	
	$output .="<tr class='firstrowth'>
			<td style='border-bottom: medium none;' colspan='2'>
				<strong class='form_group'>" . __( 'Forms Sent to Gateway - Billing', 'wpsc' ) . "</strong>
			</td>
		</tr>

		<tr>
			<td>" . __( 'Billing First Name Field', 'wpsc' ) . "</td>
			<td>
				<select name='ccavenue_form[first_name]'>
				" . nzshpcrt_form_field_list(get_option('ccavenue_form_first_name')) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing Last Name Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[last_name]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_last_name' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing Address Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[address]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_address' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing City Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[city]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_city' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing State Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[state]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_state' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing Postal/ZIP Code Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[post_code]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_post_code' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing Country Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[country]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_country' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Billing Customer Phone', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[phone]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_phone' ) ) . "
				</select>
			</td>
		</tr>
		
		";
		
		
		
		
		$output .="<tr class='firstrowth'>
			<td style='border-bottom: medium none;' colspan='2'>
				<strong class='form_group'>" . __( 'Forms Sent to Gateway - Shipping ', 'wpsc' ) . "</strong>
			</td>
		</tr>

		<tr>
			<td>" . __( 'Shipping First Name Field', 'wpsc' ) . "</td>
			<td>
				<select name='ccavenue_form[shipping_first_name]'>
				" . nzshpcrt_form_field_list(get_option('ccavenue_form_shipping_first_name')) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping Last Name Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_last_name]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_last_name' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping Address Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_address]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_address' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping City Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_city]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_city' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping State Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_state]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_state' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping Postal/ZIP Code Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_post_code]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_post_code' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping Country Field', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_country]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_country' ) ) . "
				</select>
			</td>
		</tr>
		<tr>
			<td>" . __( 'Shipping Customer Phone', 'wpsc' ) . "</td>
			<td><select name='ccavenue_form[shipping_phone]'>
				" . nzshpcrt_form_field_list( get_option ( 'ccavenue_form_shipping_phone' ) ) . "
				</select>
			</td>
		</tr>
		
		";
		
		
		
		
		
	return $output;
}

function _wpsc_filter_ccavenue_merchant_customer_notification_raw_message( $message, $notification ) {
	$purchase_log = $notification->get_purchase_log();

	if ( $purchase_log->get( 'gateway' ) == 'wpsc_merchant_ccavenue' )
		$message = get_option( 'payment_instructions', '' ) . "\r\n" . $message;

	return $message;
}

function _wpsc_ccavenue_callback(){
	 $WorkingKey= get_option('working_key');
            $Merchant_Id= $_REQUEST['Merchant_Id'];
            $Amount= $_REQUEST['Amount'];
            $Order_Id= $_REQUEST['Order_Id'];
            $Merchant_Param= $_REQUEST['Merchant_Param'];
            $Checksum= $_REQUEST['Checksum'];
            $AuthDesc= $_REQUEST['AuthDesc'];
	    
	    $ccavenue=new wpsc_merchant_ccavenue();
            $Checksum = $ccavenue->verifychecksum($Merchant_Id,$Order_Id,$Amount,$AuthDesc,$Checksum,$WorkingKey);
            //$Checksum="true";
	    if($Checksum=="true" && $AuthDesc=="Y")
	    
            {
		wpsc_update_purchase_log_status( $Order_Id, 3 );
		transaction_results( $Order_Id, false );
	    }
	//print_r($_POST);exit;
}

add_filter(
	'wpsc_purchase_log_customer_notification_raw_message',
	'_wpsc_filter_ccavenue_merchant_customer_notification_raw_message',
	10,
	2
);

if ( isset( $_REQUEST['ccavenue_callback'] ) && $_REQUEST['ccavenue_callback'] )
	add_action( 'init', '_wpsc_ccavenue_callback' );

function gateway_ccavenue($seperator, $sessionid){
	
	
	
}

