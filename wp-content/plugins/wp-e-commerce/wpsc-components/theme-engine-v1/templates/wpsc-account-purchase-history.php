<?php
/**
 * The Account > Purchase History template.
 *
 * Displays the user's order history.
 *
 * @package WPSC
 * @since WPSC 3.8.10
 */
global $col_count; ?>

<?php if ( wpsc_has_purchases() ) : ?>

 <div class="transation-wrapp">
				
			<?php if ( wpsc_has_purchases_this_month() ) : ?>
					<div class="no-more-tables">
					 <table class="table">
						<thead>
							<tr>
							<th ><?php _e( 'Status', 'wpsc' ); ?></th>
							<th class="text-center"><?php _e( 'Date', 'wpsc' ); ?></th>
							<th class="text-center"><?php _e( 'Price', 'wpsc' ); ?></th>
			
							<?php if ( get_option( 'payment_method' ) == 2 ) : ?>
			
								<th class="text-center"><?php _e( 'Payment Method', 'wpsc' ); ?></th>
			
							<?php endif; ?>
							</tr>
							
			
						</thead>
						<tbody>
						<?php wpsc_user_purchases(); ?>
						</tbody>
						
					 </table>
					</div>
					<?php else : ?>
					<div class="history-box">
							
				
						<div><?php _e( 'No transactions for this month.', 'wpsc' ); ?></div>
				
					</div>
					<?php endif; ?>
		
			
		
		<?php else : ?>
		
			<div class="history-box">
				
					<div><?php _e( 'There have not been any purchases yet.', 'wpsc' ); ?></div>
				
			</div>
		
		<?php endif; ?>
				
				
				
</div>



