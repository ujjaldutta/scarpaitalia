<?php
/**
 * The Account > Edit Profile template.
 *
 * Displays the user account page.
 *
 * @package WPSC
 * @since WPSC 3.8.10
 */
?>

<form method="post">
	

		<?php wpsc_display_form_fields(); ?>

	
	<div class="formBox Btnpart">
		<dl class="contList">
		<dd>
				<input type="hidden" value="true" name="submitwpcheckout_profile" />
				<input type="submit" value="<?php _e( 'Save Profile', 'wpsc' ); ?>" name="submit" class="cmnBtn" />
		</dd>
		</dl>
	</div>



</form>