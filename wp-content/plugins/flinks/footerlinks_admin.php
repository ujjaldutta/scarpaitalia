<?php
    if($_POST['oscimp_hidden'] == 'Y') {
        //Form data sent
        $product1_title = $_POST['product1_title'];update_option('product1_title', $product1_title);
        $product1_link = $_POST['product1_link'];update_option('product1_link', $product1_link);
        
        $product2_title = $_POST['product2_title'];update_option('product2_title', $product2_title);
        $product2_link = $_POST['product2_link'];update_option('product2_link', $product2_link);
        
        $product3_title = $_POST['product3_title'];update_option('product3_title', $product3_title);
        $product3_link = $_POST['product3_link'];update_option('product3_link', $product3_link);
        
        $product4_title = $_POST['product4_title'];update_option('product4_title', $product4_title);
        $product4_link = $_POST['product4_link'];update_option('product4_link', $product4_link);
        
        $product5_title = $_POST['product5_title'];update_option('product5_title', $product5_title);
        $product5_link = $_POST['product5_link'];update_option('product5_link', $product5_link);
        
        $facebook_url = $_POST['facebook_url'];update_option('facebook_url', $facebook_url);
        $twitter_url = $_POST['twitter_url'];update_option('twitter_url', $twitter_url);
         $insta_url = $_POST['insta_url'];update_option('insta_url', $insta_url);
        
        
        ?>
        <div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
        <?php
    } else {
         $product1_title = get_option('product1_title');$product1_link = get_option('product1_link');
         $product2_title = get_option('product2_title');$product2_link = get_option('product2_link');
         $product3_title = get_option('product3_title');$product3_link = get_option('product3_link');
         $product4_title = get_option('product4_title');$product4_link = get_option('product4_link');
         $product5_title = get_option('product5_title');$product5_link = get_option('product5_link');
         $facebook_url = get_option('facebook_url');
         $twitter_url = get_option('twitter_url');
         $insta_url = get_option('insta_url');
         
    }
?>


<div class="wrap">
    <?php    echo "<h2>" . __( 'Footer Link Management', 'oscimp_trdom' ) . "</h2>"; ?>
     
    <form name="oscimp_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
       <input type="hidden" name="oscimp_hidden" value="Y">
        <?php    echo "<h4>" . __( 'Footer product links', 'oscimp_trdom' ) . "</h4>"; ?>
        <p><?php _e("Product 1: " ); ?><input type="text" name="product1_title" value="<?php echo $product1_title; ?>" size="20">
        <?php _e("Product 1 Link: " ); ?><input type="text" name="product1_link" value="<?php echo $product1_link; ?>" size="60">
        </p>
        <p><?php _e("Product 2: " ); ?><input type="text" name="product2_title" value="<?php echo $product2_title; ?>" size="20">
        <?php _e("Product 2 Link: " ); ?><input type="text" name="product2_link" value="<?php echo $product2_link; ?>" size="60">
        </p>
        <p><?php _e("Product 3: " ); ?><input type="text" name="product3_title" value="<?php echo $product3_title; ?>" size="20">
        <?php _e("Product 3 Link: " ); ?><input type="text" name="product3_link" value="<?php echo $product3_link; ?>" size="60">
        </p>
        <p><?php _e("Product 4: " ); ?><input type="text" name="product4_title" value="<?php echo $product4_title; ?>" size="20">
        <?php _e("Product 4 Link: " ); ?><input type="text" name="product4_link" value="<?php echo $product4_link; ?>" size="60">
        </p>
        <p><?php _e("Product 5: " ); ?><input type="text" name="product5_title" value="<?php echo $product5_title; ?>" size="20">
        <?php _e("Product 5 Link: " ); ?><input type="text" name="product5_link" value="<?php echo $product5_link; ?>" size="60">
        </p>
        <hr />
        <?php    echo "<h4>" . __( 'Footer Social Links', 'oscimp_trdom' ) . "</h4>"; ?>
        <p><?php _e("Facebook URL: " ); ?><input type="text" name="facebook_url" value="<?php echo $facebook_url; ?>" size="50"></p>
        <p><?php _e("Twitter URL: " ); ?><input type="text" name="twitter_url" value="<?php echo $twitter_url; ?>" size="50"></p>
        <p><?php _e("Instagram URL: " ); ?><input type="text" name="insta_url" value="<?php echo $insta_url; ?>" size="50"></p>
         
     
        <p class="submit">
        <input type="submit" name="Submit" value="<?php _e('Update Options', 'oscimp_trdom' ) ?>" />
        </p>
    </form>
</div>