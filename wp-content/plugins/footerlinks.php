<?php
    /*
    Plugin Name: Footer Links
    Plugin URI: http://www.unifiedinfotech.net
    Description: Plugin for footer link management
    Author: Ujjal Dutta
    Version: 1.0
    Author URI: http://www.unifiedinfotech.net
    */

function footerlinks_admin_actions() {
    add_options_page("Footer links", "Manage Footer", 1, "ManageFooter", "footerlinks_admin");
}
 
add_action('admin_menu', 'footerlinks_admin_actions');

function footerlinks_admin() {
  
    include('flinks/footerlinks_admin.php');
}


?>

