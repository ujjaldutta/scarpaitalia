<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
$args = array( 
  'post_type' => 'wpsc-produc', 

  'orderby' => 'title',
  'order' => 'ASC',
  'posts_per_page' => 6, //Limits the amount of posts on each page
  's' => trim($_REQUEST['s'])
);
query_posts( $args );
//echo $GLOBALS['wp_query']->request;
?>
<section class="innerContPanel">
			<?php if (  have_posts() ) : ?>
		
			
				<h1><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h1>
				<ul class="prodList clearfix">
					<?php
					// Start the Loop.
					while (  have_posts() ) : the_post();?>
					
					<li>
						<div class="prodBox clearfix">
							<div class="prodContainer">
								<div class="prodImg">
								<?php if(wpsc_the_product_thumbnail()) :
								?>
									<a rel="<?php echo wpsc_the_product_title(); ?>" class="<?php echo wpsc_the_product_image_link_classes(); ?>" href="<?php echo esc_url( wpsc_the_product_image() ); ?>">
										<img class="product_image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php echo wpsc_the_product_title(); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo wpsc_the_product_thumbnail(); ?>"/>
		
									</a>
								<?php else: ?>
										<a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
										<img class="no-image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php esc_attr_e( 'No Image', 'wpsc' ); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo WPSC_CORE_THEME_URL; ?>wpsc-images/noimage.png" width="<?php echo get_option('product_image_width'); ?>" height="<?php echo get_option('product_image_height'); ?>" />
										</a>
								<?php endif; ?>
								
								</div>
								<div class="prodCont">
									<h3>
									<?php if(get_option('hide_name_link') == 1) : ?>
										<?php echo wpsc_the_product_title(); ?>
									<?php else: ?>
										<a class="wpsc_product_title" href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>"><?php echo wpsc_the_product_title(); ?></a>
									<?php endif; ?>
									</h3>
									<div class="prodTxt">
									
										<?php if(wpsc_the_product_additional_description()) : ?>
										<?php echo wpsc_the_product_additional_description(); ?>
										<?php else:?>
										
											<?php echo substr(strip_tags(wpsc_the_product_description()),0,300); ?>
										
										<?php endif; ?>
									
									</div>
									<?php if( wpsc_show_stock_availability() ): ?>
									<div class="boldOrangeText">
										<?php if(wpsc_product_has_stock()) : ?>
											<div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="in_stock"><?php _e('Product in stock', 'wpsc'); ?></div>
										<?php else: ?>
											<div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="out_of_stock"><?php _e('Product not in stock', 'wpsc'); ?></div>
										<?php endif; ?>
									</div>
									<?php endif; ?>
								
									<div class="boldOrangeText">
									
											<?php if(wpsc_product_is_donation()) : ?>
												<label for="donation_price_<?php echo wpsc_the_product_id(); ?>"><?php _e('Donation', 'wpsc'); ?>: </label>
												<input type="text" id="donation_price_<?php echo wpsc_the_product_id(); ?>" name="donation_price" value="<?php echo wpsc_calculate_price(wpsc_the_product_id()); ?>" size="6" />
			
											<?php else : ?>
												<?php wpsc_the_product_price_display(); ?>
			
												<!-- multi currency code -->
												<?php if(wpsc_product_has_multicurrency()) : ?>
													<?php echo wpsc_display_product_multicurrency(); ?>
												<?php endif; ?>
											<?php endif; ?>
									
									</div>
									<div class="">
										<a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>" class="blueBtn buy">Buy Now</a>
										<a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>" class="blackBtn see">See Details</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					
					<?php
					endwhile;
					// Previous/next post navigation.
					twentyfourteen_paging_nav();
					
					?>
					
				</ul>
			
			
			<?php
			else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
</section>
			



<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();
