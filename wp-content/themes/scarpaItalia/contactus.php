<?php
/**
 * Template Name:Contact Us
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

 get_header();
 $page_id = 38;
 $page_data = get_page( $page_id );
?>
<section class="innerContPanel contForm">
<h1><?php echo $page_data->post_title?></h1>
<h2>Contact Form</h2>

<div class="row">
    <div class="col-md-6 col-sm-6">
			    <?php
                            // Start the Loop.
				while ( have_posts() ) : the_post();
                            // Include the page content template.
					get_template_part( 'content', 'page' );
                                endwhile;        
                                        ?>
    </div>
    <div class="col-md-6 col-sm-6">
        
        <div class="padL24">
                <div class="cont-phoneBar mb34">
                        <h2>
                                <?php if ( is_active_sidebar( 'contact_us' ) ) : ?>
                                <?php dynamic_sidebar( 'contact_us' ); ?>
                                <?php endif; ?>
                        </h2>
                </div>
               <?php twentyfourteen_post_thumbnail();?>
        </div>
        
        
            
    </div>
    
    
</div>
</section>			

<?php
//get_sidebar();
get_footer();

?>
