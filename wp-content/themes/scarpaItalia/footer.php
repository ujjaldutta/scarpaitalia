<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
		</div>
	</section>
	<footer class="container footerPanel">
		<div class="footer">			
			<section class="footTopSection">				
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<h4>Products</h4>
						<ul class="footNav">
							<?php $p1=get_option('product1_title');
							$l1=get_option('product1_link');
							if($p1){
							?>
							<li><a href="<?php echo $l1?>"><?php echo $p1?></a></li><?php }?>
							
							<?php $p1=get_option('product2_title');
							$l1=get_option('product2_link');
							if($p1){
							?>
							<li><a href="<?php echo $l1?>"><?php echo $p1?></a></li><?php }?>
							
							<?php $p1=get_option('product3_title');
							$l1=get_option('product3_link');
							if($p1){
							?>
							<li><a href="<?php echo $l1?>"><?php echo $p1?></a></li><?php }?>
							<?php $p1=get_option('product4_title');
							$l1=get_option('product4_link');
							if($p1){
							?>
							<li><a href="<?php echo $l1?>"><?php echo $p1?></a></li><?php }?>
							<?php $p1=get_option('product5_title');
							$l1=get_option('product5_link');
							if($p1){
							?>
							<li><a href="<?php echo $l1?>"><?php echo $p1?></a></li><?php }?>
						</ul>
					</div>
					<div class="col-md-3 col-sm-3">
						<h4>SHIPPING &amp; POLICY</h4>
						<ul class="footNav">
							<li><a href="<?php echo get_permalink( 71 ); ?>">Safe and Secure Shopping</a></li>
							<li><a href="<?php echo get_permalink( 73 ); ?>">Disclaimer</a></li>
							<li><a href="<?php echo get_permalink( 75 ); ?>">Help &amp; FAQs</a></li>
							<li><a href="<?php echo get_permalink( 77 ); ?>">Privacy Policy</a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-3">
						<h4>Connect with us</h4>
						<ul class="socialFootLinks">
							<li><a href="<?php echo get_option('facebook_url')?>" target="_blank"><i class="fa fa-facebook"></i> <label>Facebook</label></a></li>
							<li><a href="<?php echo get_option('twitter_url')?>" target="_blank"><i class="fa fa-twitter"></i> <label>Twitter</label></a></li>
							<li><a href="<?php echo get_option('insta_url')?>" target="_blank"><i class="fa fa-instagram"></i> <label>Instagram</label></a></li>
						</ul>
					</div>
					
                                        
					
                                        
					
					
					<div class="col-md-4 col-sm-4">
						
						<script type="text/javascript">
					//<![CDATA[
					if (typeof newsletter_check !== "function") {
					window.newsletter_check = function (f) {
					    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
					    if (!re.test(f.elements["ne"].value)) {
						alert("The email is not correct");
						return false;
					    }
					    if (f.elements["ny"] && !f.elements["ny"].checked) {
						alert("You must accept the privacy statement");
						return false;
					    }
					    return true;
					}
					}
					
					jQuery(document).ready(function(){
					
					jQuery("#newslettersubmit").click(function(){
						jQuery(".ajax-loader").show();
						jQuery("#nmsg").show();
						jQuery.ajax(
							{
							    url : '<?php echo site_url()?>/wp-content/plugins/newsletter/do/subscribe.php',
							    type: "POST",
							    data : jQuery("#nform").serialize(),
							    success:function(data, textStatus, jqXHR)
							    {
								//data: return data from server
								//alert(data);
								jQuery("#nmsg").html(data);
								setTimeout(function() {
										jQuery("#nmsg").html('&nbsp;');
										jQuery("#nmsg").hide('slow');
									    }, 5000);
								jQuery(".ajax-loader").hide();
							    },
							    error: function(jqXHR, textStatus, errorThrown)
							    {
								jQuery(".ajax-loader").hide();
							    }
							});

						
						return false;
						});
					
						
											
						  /* Main Nav */
						  jQuery( ".phoneNavBtn" ).click(function() {
							  jQuery( ".main-nav" ).slideToggle( "slow", function() {});
						  });
						  
						  /* Spin Value */
						  jQuery( ".spinner" ).spinner({min: 1});
	  
	  
  
						
						});
					
					
					//]]>
					</script>
						<h4>Newsletter</h4>
						 <?php if ( is_active_sidebar( 'contact_us' ) ) : ?>
						<?php dynamic_sidebar( 'newsletter' ); ?>
						<?php endif; ?>
						<form method="post" action="" id="nform" onsubmit="return newsletter_check(this)">
						<div class="subsFields">
						<input class="newsletter-email" type="text" name="ne" size="30" required placeholder="Enter your email">
						
								<input class="subscBtn fr" type="button" id="newslettersubmit" value="SIGN UP"/>
								
						</div>	
						</form>
						<img class="ajax-loader" src="<?php echo plugins_url()?>/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="display:none;">
						<div id="nmsg" style="color:#7AD03A">&nbsp;</div>
						<div class="paymentLogo">
							<img src="<?php echo get_template_directory_uri(); ?>/images/cc-avenue.png" alt="Payment Gateway">
						</div>
					</div>
				</div>
			</section> 
				
			<section class="copyrightSection">
				&copy; 2014 <a href="#">www.scarpaItalia.com</a>. All rights reserved.
			</section>
		</div>
		<?php get_sidebar( 'footer' ); ?>
	</footer>
	
   	<?php wp_footer(); ?>

  </body>
</html>