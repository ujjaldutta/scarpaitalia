<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	
	
	
	
	<?php wp_head(); ?>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,300,700,400,600' rel='stylesheet' type='text/css'>
		
	  <!-- Bootstrap -->
	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	 
	    <link href="<?php echo get_template_directory_uri(); ?>/css/screen.css" rel="stylesheet">
	    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
	    
	    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
		
</head>

<body <?php body_class(); ?>>
<span id="masthead"></span>
<header class="header">
		<div class="container">
			<div class="clearfix">
				<ul class="logLinks fr">
					<li><a href="<?php echo get_permalink( 13 ); ?>">Home</a></li>
					<li><a href="<?php echo get_permalink( 38 ); ?>">Contact Us</a></li>
					<?php if ( is_user_logged_in() ) {?>
					<li><a href="<?php echo get_permalink( 8 ); ?>">Account</a></li>
					<li><a href="<?php echo wp_logout_url( site_url() ); ?>">Logout</a></li>
					<?php }else{?>
					<li><a href="<?php echo get_permalink( 41 ); ?>">Register</a></li>
					<li><a href="<?php echo get_permalink( 45 ); ?>">Login</a></li>
					<?php }?>
				</ul>
			</div>
			<section class="hdrTop clearfix">
				<div class="fl logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt=""></a>
				</div>
				<div class="fr">
					<ul class="searchItems">
						<li class="stampList"><img src="<?php echo get_template_directory_uri(); ?>/images/stamp.png" alt="" class="stamp"></li>
						<li>
							<div class="inputBox">
								<form name="hdr-search" action="<?php echo site_url();?>/" method="get" >
									<input type="search" value="" placeholder="Search for a product" name="s">
									
									<input type="submit" value="" class="searchBtn" title="Click to Search">
								</form>
								<?php //get_search_form(); ?>
								
								
							</div>
						</li>
						<li>
							<div class="inputBox">
								<a href="<?php echo get_permalink( 6 ); ?>" class="cartBtn"></a>
								<span class="cartDtls">(<?php echo(wpsc_cart_item_count()); ?> Items) - <?php echo(wpsc_cart_total()); ?></span>
							</div>
						</li>
					</ul>
				</div>
			</section>
			
			<section class="related navAreaBox">
				<div class="navArea clearfix">
					<div class="phone">
						<a href="javascript:void(0);" class="phoneNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</a>
					</div>
					
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'main-nav' ) ); ?>
				</div>
				<a href="<?php echo get_permalink( 81 ); ?>" class="newArivBtn"></a>
			</section>			
		</div>
	</header>
		
	
	<section class="container bodyArea">
		<div class="bodyPanel">
			
<?php
//echo $currency_type = get_option( 'currency_type' );
    //$country       = new WPSC_Country( $currency_type );

    //echo $ct_code = $country->get_currency_code();   // Country currency code
    //echo $ct_symb = $country->get_currency_symbol(); // Country symbol
    //exit;
    //update_option("currency_type",136);
    
  //  $curr = new CURRENCYCONVERTER();
//echo $rate = $curr->convert( 1, "EUR", "USD" );exit;
?>