<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<section class="innerContPanel">
	<?php if(!is_page( 38 )){?>
	<?php
	if ( !is_front_page()  ) {
		the_title( '<h1 class="">', '</h1>' );
	}
	?>
	
	
	<div class="fl"><?php // Page thumbnail and title.
		twentyfourteen_post_thumbnail();?></div>
		
	<?php }?>

	<section class="clearfix">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );

			edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
		?>
	</section><!-- .entry-content -->
</section><!-- #post-## -->
